$(document).ready(()=>{
    /*Переключение отображения каталоге*/
    $('.grid-mode').click(()=>{
        $(".row-mode").removeClass("active-mode")
        $(".grid-mode").addClass("active-mode")
        $('.view-row').css('display', 'none')
        $('.view-grid').css('display', 'grid')
    })

    $('.close-result--coupon__message').click(function(){
        $(this).closest('.result--coupon__message').fadeOut();
    })

    $('.row-mode').click(()=>{
        $(".grid-mode").removeClass("active-mode")
        $(".row-mode").addClass("active-mode")
        $('.view-grid').css('display', 'none')
        $('.view-row').css('display', 'flex')
    })
    /*END*/

    $('body').on('click','.search-mobile-icon', function(){
        $('.search-mobile-popup').show();
    });

    $('body').on('click','.search-mobile-popup__close', function(){
        $('.search-mobile-popup').hide();
    });

    /*Очистить поле поиска*/
    $('.icon-x-wrapper').click(function () {
        $('.search input').val('')
    });
    /*END*/

    /*Установка цветов на странице товара и в быстром просмотре на странице каталога и в настроках корзины*/
    $('.colors>div p>span, .colors>div label>span').each((k, v)=>{
        $(v).css('background-color', $(v).attr("color"))
    });

    $('.main-view').on('click', function(){
        $('.second-view').removeClass('d-flex');
        $(this).next().toggleClass('d-flex');
    });
    /*END*/

    /*Переключение табов на странице товара*/
    $('.product-info .tabs-btn .tab-btn').click(function (){
        let n = $(this).attr('tab')
        $('.product-info .tabs-btn .tab-btn').each((k, v)=> {
            $(v).removeClass("active-tab-btn")
        })

        $(this).addClass('active-tab-btn')
        $(".tab").css('display', 'none')
        $(".tab-"+n).css('display', 'flex')
    })
    /*END*/

    /*Переключение табов на странице "Информация о доставке и оплате"*/
    $('.page-content .tabs-btn .tab-btn').click(function (){
        let n = $(this).attr('tab')
        $('.page-content .tabs-btn .tab-btn').each((k, v)=> {
            $(v).removeClass("active-tab-btn")
        })

        $(this).addClass('active-tab-btn')
        $(".tab").css('display', 'none')
        $(".tab-"+n).css('display', 'block')
    })
    /*END*/

    /*Переключение табов на странице Аккаунт*/
    $('.menu-item').click(function() {
        $('.menu-item').removeClass('menu-item-active')
        $(this).addClass('menu-item-active')
        $('.right-content>div').css('display', 'none')
        let className = $(this).attr("class").split(' ')[1];
        $(`.${className}-content`).css('display', 'flex')
    })
    /*END*/
    /*Открытие спойлера на странице Аккаунт в табе Мои заказы*/
    $('.more').click(function() {
        $(this).toggleClass('more-active')
        let spoilerId = $(this).attr('spoiler')
        $(`#${spoilerId}`).toggleClass('open-gap')
    })
    /*END*/

    /*Открытие спойлера на странице Аккаунт в табе Профили заказов*/
    $('.edit').click(function() {
        let spoilerId = $(this).attr('spoiler')
        $(`#${spoilerId}`).toggleClass('open-gap')
    })
    /*END*/

    /*Переключение полей с просмотра на редактирование (на против которых стоит карандаш)*/
    $('.name--value .field .value .icon-wrapper:not(.pass-icon)').click(function() {
        let parent = $(this).parent('.value')
        parent.find('span').toggleClass('hidden-val')
        parent.find('input').toggleClass('show-val').focus()
        parent.find('textarea').toggleClass('show-val')
        $(this).toggleClass('hidden-pencil')
    })

    $('.name--value .field .value input:not(input[type=radio])').keypress(function (e) {
        if (e.which === 13) {
            let parent = $(this).parent('.value')
            parent.find('span').toggleClass('hidden-val')
            parent.find('input').toggleClass('show-val')
            parent.find('textarea').toggleClass('show-val')
            parent.find('.icon-wrapper').toggleClass('hidden-pencil')
            return false;
        }
    });

    /*$('*:not(.name--value .field .value .icon-wrapper)').click(function() {
        let parent = $('.name--value .field .value')
        parent.find('span').toggleClass('hidden-val')
        parent.find('input').toggleClass('show-val')
        parent.find('textarea').toggleClass('show-val')
        parent.find('.icon-wrapper').toggleClass('hidden-pencil')
    })*/
    $('.name--value .field .value input').blur(function () {
        let parent = $(this).parent('.value')
        parent.find('span').toggleClass('hidden-val')
        parent.find('input').toggleClass('show-val')
        parent.find('textarea').toggleClass('show-val')
        parent.find('.icon-wrapper').toggleClass('hidden-pencil')
    })


    $('.name--value .field .value .pass-icon').click(function() {
        $('.password-container').css('display', 'flex')
    })
    $('.password-container .icon-wrapper').click(function() {
        $('.password-container').css('display', 'none')
    })
    $('.password-container').click(function (e) {
        if($(e.target).is('.password-container')){
            $('.password-container').css({display: 'none'})
        }
    })
    /*END*/

    /*Автомотический скролинг на странице оформления заказа*/
    /*Вниз*/
    for (let i = 1; i <= 5; i++) {
        $(`.ordering-content .left-block .step-${i} .step-foot .next`).click(function() {
            $(`.ordering-content .left-block .step-${i} .step-body`).css('display', 'none')
            let  option = (i === 3) ? 'flex' : 'block'
            $(`.ordering-content .left-block .step-${i+1} .step-body`).css('display', option)

            $('html, body').animate({
                scrollTop: $(`.step-${i+1}`).offset().top
            }, 1500);
        })
    }

    /*Вверх*/
    for (let i = 5; i >= 1; i--) {
        $(`.ordering-content .left-block .step-${i} .step-foot .back`).click(function () {
            $(`.ordering-content .left-block .step-${i} .step-body`).css('display', 'none')
            let  option = (i === 5) ? 'flex' : 'block'
            $(`.ordering-content .left-block .step-${i-1} .step-body`).css('display', option)
            $('html, body').animate({
                scrollTop: $(`.step-${i-1}`).offset().top
            }, 1500);
        })
    }
    /*END*/

    /*Изменение отдельных пунктов на странице оформления заказа "Нопка изменить"*/
    $('.ordering-content .left-block .step .step-head .change-step').click(function() {
        let parent = $(this).parents('.step')
        let className = $(parent).attr("class").split(' ')[1];
        let  option = (className === 'step-3' || className === 'step-4') ? 'flex' : 'block'
        if (className === 'step-3') {
            $(`.${className} .step-body>div`).css('display', option)
        }
        $(`.${className} .step-body`).css('display', option)
    })
    /*END*/

    /*Фиксированная позиция праваго блока на странцие оформление заказа*/
    // $(document).scroll((e) => {
    //     var postion = $(document).scrollTop();
    //     if (postion > 265) {
    //         $('.ordering-content .right-block .result--coupon').css('position', 'fixed')
    //     } else {
    //         $('.ordering-content .right-block .result--coupon').css('position', 'initial')
    //     }
    // })
    /*END*/

    /*Настройка слайдера*/
        let countSlides = $('.slide').length
        for (let i = 0; i < countSlides; i++) {
            if (i === 0) {
                $('.dots').append(`<span class="dot current-dot" dot="${i}"></span>`)
            } else {
                $('.dots').append(`<span class="dot" dot="${i}"></span>`)
            }
        }

        $('.slider-wrapper').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            autoplay: true,
            autoplaySpeed: 2000,
            responsive: [
            {
              breakpoint: 990,
              settings: {
                dots: false
              }
            }
            ]
        })

        $('.dots .dot').click(function () {
            let dotIndex = $(this).attr('dot')
            $('.slider-wrapper').slick('slickGoTo', index = dotIndex)
        })

        $('.arrows .slide-prev').click(function () {
            $('.slider-wrapper').slick('slickPrev')
        })
        $('.arrows .slide-next').click(function () {
            $('.slider-wrapper').slick('slickNext')
        })

        $('.slider-wrapper').on('afterChange', function(event, slick, currentSlide, nextSlide){
            $(`span.dot`).removeClass('current-dot')
            $(`span[dot=${currentSlide}]`).addClass('current-dot')
        });
    /*END*/

    /*Настройка открытия окн личного кабинета, авторизации и регистрации*/
        $('.user-icon-wrapper').click(function() {
            $('.favorite-icon-wrapper, .basket-icon-wrapper').removeClass('opened');
            $('.modal-window').removeClass('modal-window_open');
            $(this).toggleClass('active-user');
            if ($(this).hasClass('active-user')) {
                $('.auth-false').addClass('show-lk-auth')
            } else {
                $('.auth-false').removeClass('show-lk-auth')
                $('.lk, .auth').removeClass('show-lk-auth')
            }
        }/*, () => {
            $('.lk, .auth').removeClass('show-lk-auth')
            $('.auth-false').addClass('show-lk-auth')
        }*/)
        $('.login-btn').click(()=>{
            $('.lk, .auth').removeClass('show-lk-auth')
            $('.login').addClass('show-lk-auth')
        })
        $('.reg-btn').click(()=>{
            $('.lk, .auth').removeClass('show-lk-auth')
            $('.reg').addClass('show-lk-auth')
        })

    /*END*/

    /*Настройка открытия окна "Связаться с нами"*/
        $('.request-call-btn').click(()=>{
            $('.tel-drop-info').css({display: 'none'})
            $('.request-call-form').css({display: 'flex'})
        })
    /*END*/

    /*Настройка закрытия всех всплывающих окн*/
        $('.user .close').click(function () {
            // $(this).parents('.lk, .auth').removeClass('show-lk-auth')
            $('.user-icon-wrapper').trigger('click');
            $('body').css('overflow', 'auto')
        })

        $('.favorite .close-modal, .basket .close-modal').click(function () {
            // $(this).parents('.modal-window').css({display: 'none'})
            $('body').css('overflow', 'auto')
            if ($(this).closest('.favorite').length) {
                $(this).closest('.favorite').find('.favorite-icon-wrapper').trigger('click')
            } else {
                $(this).closest('.basket').find('.basket-icon-wrapper').trigger('click')
            }
        })

        $('.tel-drop-content .close-info').click(function () {
            $('.tel-drop-info').css({display: 'none'})
            $('.request-call-form').css({display: 'none'})
        })

        $('.favorite-icon-wrapper, .basket-icon-wrapper').click(function() {
            // $('.modal-window').attr('style', '')
            if ($('.user-icon-wrapper').hasClass('active-user')) {
                $('.user-icon-wrapper').trigger('click');
            }
            if ($(this).hasClass('basket-icon-wrapper')) {
                $('.favorite .modal-window').removeClass('modal-window_open');
                $('.favorite-icon-wrapper').removeClass('opened');
            }
            else {
                $('.basket .modal-window').removeClass('modal-window_open');
                $('.basket-icon-wrapper').removeClass('opened');
            }
            $('.modal-window').removeClass('modal-window_open');
            $(this).toggleClass('opened');
            if ($(this).hasClass('opened')) {
                $(this).parent().find('.modal-window').addClass('modal-window_open');
                if ($(window).innerWidth() < 990)
                    $('body').css('overflow', 'hidden')
            } else {
                $(this).parent().find('.modal-window').removeClass('modal-window_open');
                if ($(window).innerWidth() < 990)
                    $('body').css('overflow', 'auto')
            }
        }/*, () => {
        }*/)

        $('.tel-drop').hover(()=>{
            if ($(window).outerWidth() > 991) {
                $('.request-call-form, .tel-drop-info').attr('style', '')
            }
        }, () => {
        })
    /*END*/

    /*Настройка открытия окна "Связаться с нами" по кнопке из футера*/
        $('.request-call').click(()=>{
            $('.overlay-rc').css({display: 'flex'})
        })
        $('.overlay .close-info').click(function () {
            $('.overlay-rc').css({display: 'none'})
        })
        $('.overlay-rc').click(function (e) {
            if($(e.target).is('.overlay-rc')){
                $('.overlay-rc').css({display: 'none'})
            }
        })
    /*END*/

    /*Открытие и закрытие окна пользовательского соглашения*/
        $('.check-container a').click(()=>{
            $('.overlay-agreement').css({display: 'flex'})
        })
        $('.agreement .head .icon-wrapper').click(()=>{
            $('.overlay-agreement').css({display: 'none'})
        })
        $('.overlay-agreement').click(function (e) {
            if($(e.target).is('.overlay-agreement')){
                $('.overlay-agreement').css({display: 'none'})
            }
        })
    /*END*/

    /*Кнопка To Top*/
        $('.to-top').click(()=>{
            $('html,body').animate({ scrollTop: 0 }, 'slow');
            return false;
        })

        $( window ).scroll(function() {
            // var scroll = $(window).scrollTop();
            // let o = -(1 - scroll / 300)
            // if (o > 0.5) {
            //     o = 0.5;
            // }
            // $('.to-top-wrapper').css({opacity: o})
            if ($(window).scrollTop() + $(window).height() > $('footer').offset().top - 150 ) {
                $('.to-top-wrapper').addClass('visible');
            } else {
                $('.to-top-wrapper').removeClass('visible');
            }
        });
    /*END*/

    /*Фильтр дороже дешевле*/
    let priceFilter = false
    $('.price-filter').click(()=>{
        if (!priceFilter) {
            $('.price-filter .icon-wrapper i').attr('icon', 'cheaper').empty()
            icons.cheaper.make()
            $('.price-filter > span').text('Дешевле')
            priceFilter = true
        } else {
            $('.price-filter .icon-wrapper i').attr('icon', 'expensive').empty()
            $('.price-filter > span').text('Дороже')
            icons.expensive.make()
            priceFilter = false
        }

    })
    /*END*/
    /*Открытие/Закрытие быстрого просмотра*/
    $('.quick-view').click(function(e) {
        // e.stopPropagation()
        // e.preventDefault();
        // let url = $(this).attr("href")
        // $.get(url)
        //     .done(function( data ) {
        //       $('.quick-viewer-container').empty().append(data).css({display: 'flex'})
        // });
        $('.quick-viewer-container').css({display: 'flex'}).find('.slick-initialized').slick('refresh');

    })
    $('.quick-viewer-container .close-btn-container').click(function () {
        $('.quick-viewer-container').css({display: 'none'})
    })
    $('.quick-viewer-container').click(function (e) {
        if($(e.target).is('.quick-viewer-container')){
            $('.quick-viewer-container').css({display: 'none'})
        }
    })
    /*END*/

    /*Выбор партии в быстром просмотре (пункт "Количество в упаковке")*/
    $('.quantity > div > p:not(.unavailable), .quantity > div > label:not(.unavailable)').click(function() {
        $('.quantity > div > p:not(.unavailable)').removeClass('active')
        $('.quantity > div > label:not(.unavailable)').removeClass('active')
        $(this).addClass('active')
    })
    /*END*/
    /*Переключение фотографий в быстром просмотре*/
    $('.thumbnail-wrapper img').click(function() {
        let src = $(this).attr('src')
        $('.main-photo .photo-wrapper img').attr('src', src)
    })
    /*END*/
    /*Выбор размера (пункт "Размер")*/
    $('.dimensions > div > p:not(.unavailable), .dimensions > div > label:not(.unavailable)').click(function() {
        $('.dimensions > div > p:not(.unavailable)').removeClass('active')
        $('.dimensions > div > label:not(.unavailable)').removeClass('active')
        $(this).addClass('active')
    })
    /*END*/
    /*Выбор цвета (пункт "Цвет")*/
    $('.colors > div > p:not(.unavailable), .colors > div > label:not(.unavailable)').click(function() {
        $('.colors > div > p:not(.unavailable)').removeClass('active')
        $('.colors > div > label:not(.unavailable)').removeClass('active')
        $(this).addClass('active')
    })
    /*END*/
    /*Закрытие окна выбора города*/
    $('.city-drop-content .city-drop-head .icon-wrapper').click(function() {
        $('.city-drop-content').css({display: 'none'})
    })
    $('.city-drop').hover(()=>{
        $('.city-drop-content').attr('style', '')
    }, () => {
    })
    /*END*/

    /*Назначение активного класса для кнопки Звезда(Есть в быстром просмотре и на странице товара)*/
    $('.star-btn').click(function() {
        $(this).toggleClass('active-star')
    })
    /*END*/

    /*Таймер для товара недели*/
    let time = $('#start-timer').text()
    let timer = $('#timer')
    let countDownDate = new Date(time).getTime();
    let x = setInterval(function() {
        let now = new Date().getTime();
        let distance = countDownDate - now;
        let days = Math.floor(distance / (1000 * 60 * 60 * 24));
        let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        let seconds = Math.floor((distance % (1000 * 60)) / 1000);
        if(seconds < 10) seconds = '0' + seconds
        if(minutes < 10) minutes = '0' + minutes
        if(hours < 10) hours = '0' + hours
        let showDays = days > 0 ? days + ' : ' : ''
        timer.text(`${showDays}${hours} : ${minutes} : ${seconds}`)

        if (distance < 0) {
            clearInterval(x);
            timer.text('Время вышло');
        }

    }, 1000);
    /*END*/

    /*Открытие окна заказа товара, который в данный момент отсутствует*/
    // $('.empty-product').click(function() { /*TODO убрать комментарии перед загрузкой в битрикс*/
    //     let parentId = $(this).parents('.cat-product').attr('id')
    //     $('#' + parentId + '.request-empty-product .thx').css('display', 'none')
    //     $('#' + parentId + '.request-empty-product .form-wrapper').css('display', 'block')
    //     $('#' + parentId + ' .request-empty-product-container').css('display', 'flex')
    // })

   $('.empty-product').click(function() {
        $('.request-empty-product .thx').css('display', 'none')
        $('.request-empty-product .form-wrapper').css('display', 'block')
        $('.request-empty-product-container').css('display', 'flex')
    })

    $('.open-empty-form').click(function(e) {
        e.stopPropagation()
        $('.request-empty-product .thx').css('display', 'none')
        $('.request-empty-product .form-wrapper').css('display', 'block')
        $('.request-empty-product-container').css('display', 'flex')
    })
    $('.request-empty-product .icon-wrapper').click(function() {
        $('.request-empty-product-container').css('display', 'none')
    })
    $('.request-empty-product-container').click(function (e) {
        if($(e.target).is('.request-empty-product-container')){
            $('.request-empty-product-container').css({display: 'none'})
        }
    })
    /*Показываем окно спасибо*/
    let url = window.location.href, idx = url.indexOf("#")
    let hash = idx !== -1 ? url.substring(idx+1) : "";
    if (hash === 'req-thx') {
        console.log(hash)
        $('.request-empty-product-container').css('display', 'flex')
        $('.request-empty-product .form-wrapper').css('display', 'none')
        $('.request-empty-product .thx').css('display', 'flex')
    }
    /*END*/

    /*Открытие настроек корзины*/
    $('.cat-product i[icon="basket"], .js-open-basket-settings-popup').click(function() {
        $('.basket-settings-container').css('display', 'flex')
    })
    $('.cat-product i[icon="basket"]').click(function() {
        let parentId = $(this).parents('.cat-product').attr('id')
        $('#' + parentId +' .basket-settings-container').css('display', 'flex')
    })
    $('.basket-settings .icon-wrapper').click(function() {
        $('.basket-settings-container').css('display', 'none')
    })
    $('.basket-settings-container').click(function (e) {
        if($(e.target).is('.basket-settings-container')){
            $('.basket-settings-container').css({display: 'none'})
        }
    })
    /**/

    /*Управление табами на страницах Акции и Отзывы*/

    $('.stock-type span').click(function (e) {
        $('.stock-type span').removeClass('active-stock')
        $(e.target).addClass('active-stock')
        let tab = $(e.target).attr('tab-btn')
        $('.stock-items').css('display', 'none')
        $(`div[tab=${tab}]`).css('display', 'flex')
    })
    /*END*/

    /*Управление кнопками Количество покупаемого товара*/
    $('.count .minus').click(function (e) {
        event.stopPropagation();
        let parent = $(this).parent('.count')
        let strVal = parent.find('.count-value')
        let val = parseInt(strVal.val())
        val = val - 1
        if (val <= 1) {
            strVal.val(1)
        } else {
            strVal.val(val)
        }
    })
    $('.count .plus').click(function (e) {
        event.stopPropagation();
        let parent = $(this).parents('.count')
        let strVal = parent.find('.count-value')
        let val = parseInt(strVal.val())
        val += 1
        if (val >= 999) {
            strVal.val(999)
        } else {
            strVal.val(val)
        }
    })
    $('.count .count-value').change(function (e) {
        if ($(this).val() === '') $(this).val(1)

        let n = parseInt(Number($(this).val().replace(/[a-z, ]/gi,'')),10);
        $(this).val((n === 0) ? 1 : n)

    })
    $('.count .count-value').keypress(function(evt) {
        var theEvent = evt || window.event;

        // Handle paste
        if (theEvent.type === 'paste') {
            key = event.clipboardData.getData('text/plain');
        } else {
            // Handle key press
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
        }
        var regex = /[0-9]|\./;
        if( !regex.test(key) ) {
            theEvent.returnValue = false;
            if(theEvent.preventDefault) theEvent.preventDefault();
        }
    });

    $('.count .count-value').bind("paste", function(e){
        var pastedData = e.originalEvent.clipboardData.getData('text');
        if (isNaN(pastedData)) {
            $(this).val('').val(e.target.value).val('')
        }
    } );
    /*END*/



    /*Управление окном "Нашли дешевле?"*/
    $('.found-cheaper [type="button"]').click(function() {
        $('.cheaper-form-container').css('display', 'flex')
    })
    $('.cheaper-form .icon-wrapper').click(function() {
        $('.cheaper-form-container').css('display', 'none')
    })
    $('.cheaper-form-container').click(function (e) {
        if($(e.target).is('.cheaper-form-container')){
            $('.cheaper-form-container').css({display: 'none'})
        }
    })
    /*END*/

    /*Управление комментариями*/
    /*Показать/Скрыть комментарии*/
    $('.review-container .comments').click(function() {
        $(this).toggleClass('comments-active')
        let parent = $(this).parents('.review-container')
        parent.find('.replies-container').toggleClass('hidden-replies')
    })
    /*Открытие окна оставить отзыв*/
    $('.reviews-content .header button').click(function() {
        $('.send-review-container').css('display', 'flex')
    })
    /*Открытие окна оставить отзыв*/
    $('.reviews-content .body .actions .reply-icon').click(function() {
        $('.send-reply-container').css('display', 'flex')
    })
    /*Закрытие окн*/
    $('.send-review .icon-wrapper').click(function() {
        $('.send-review-container').css('display', 'none')
    })
    $('.send-review .icon-wrapper, .send-reply .icon-wrapper').click(function() {
        $('.send-reply-container').css('display', 'none')
    })
    $('.send-review-container, .send-reply-container').click(function (e) {
        if($(e.target).is('.send-review-container')){
            $('.send-review-container').css({display: 'none'})
        }
        if($(e.target).is('.send-reply-container')){
            $('.send-reply-container').css({display: 'none'})
        }
    })
    /*END*/

    /*Управление галереей в карточке товара*/
    $('.thumbnails .thumbnail').click(function() {
        $('.thumbnails .thumbnail').removeClass('thumbnail-active')
        $(this).toggleClass('thumbnail-active')
        let src = $(this).find('img').attr('src')
        $('.product-photo .main-photo img').attr('src', src)
    })
    /*END*/

    $('.product-slider').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        dots: false,
        dotsClass: 'custom_paging',
        customPaging: function (slider, i) {
            return  (i + 1) + '/' + slider.slideCount;
        },
        // arrows: false,
        // autoplay: true,
        // autoplaySpeed: 2000,
        responsive: [
            {
              breakpoint: 1279,
              settings: {
                slidesToShow: 3,
              }
            },
            {
              breakpoint: 767,
              settings: {
                slidesToShow: 2,
              }
            },
            {
              breakpoint: 587,
              settings: {
                slidesToShow: 1,
                dots: true,
              }
            },
        ]
    });
    $('.js-thumbnails').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        dots: false,
        // arrows: false,
        // autoplay: true,
        // autoplaySpeed: 2000,
        responsive: [
            {
              breakpoint: 1279,
              settings: {
                slidesToShow: 3,
              }
            },
            {
              breakpoint: 1079,
              settings: {
                slidesToShow: 4,
              }
            },
            {
              breakpoint: 767,
              settings: {
                slidesToShow: 3,
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 2,
              }
            },
        ]
    });

    /*Добавление скрытых спанов в бейджи*/
    $('.discount').each(function () {
        $(this).append('<span class="crutch">')
    })
    /*END*/

    /*Открытие окна заказа товара, который в данный момент отсутствует*/
    $('.buy-one').click(function() {
        console.log('clicl')
        $('.buy-one-click .thx').css('display', 'none')
        $('.buy-one-click .form-wrapper').css('display', 'block')
        $('.buy-one-click-container').css('display', 'flex')
    })
    $('.buy-one-click .icon-wrapper').click(function() {
        $('.buy-one-click-container').css('display', 'none')
    })
    $('.buy-one-click-container').click(function (e) {
        if($(e.target).is('.buy-one-click-container')){
            $('.buy-one-click-container').css({display: 'none'})
        }
    })
    /*Показываем окно спасибо*/
    let url2 = window.location.href, idx2 = url.indexOf("#")
    let hash2 = idx2 !== -1 ? url2.substring(idx2+1) : "";
    if (hash2 === 'req-thx') {
        $('.buy-one-click-container').css('display', 'flex')
        $('.buy-one-click .form-wrapper').css('display', 'none')
        $('.buy-one-click .thx').css('display', 'flex')
    }
    /*END*/

    /*Открытие меню на мобилке*/

    /*$('.burger-menu').hover(()=>{
        $('.sub-catalog').attr('style', '')
    }, () => {
    }).click(function(e) {
        if ($(e.target).is('.burger-menu>span'))
            $('.sub-catalog').attr('style', '')
    })*/
    $('.burger-mobile, .close-mobile-menu').click(function(e) {
        e.stopPropagation()
        $('.sub-catalog').attr('style', '')
        $('.second-header').toggleClass('second-header-mobile')

        if (!$('.second-header').hasClass('catalog-opened')) {
            $('.second-header').removeClass('catalog-opened');
        }
        if ($(this).is('.burger-mobile')) {
          $('body').css('overflow', 'hidden')
        } else {
          $('body').css('overflow', 'auto')
        }
    })
    $('.burger-menu').click(function() {
        if ($(window).outerWidth() < 990) {        
            let firstLvl = $(this).find('.sub-catalog')[0]
            $(firstLvl).css({
                'display': 'flex',
                'left': '-2px',
                'z-index': 100
            })
            $('.second-header').addClass('catalog-opened');
        }
    })
    $('.yet-drop .yet-text').click(function() {
        let _this = $(this);
        if ($(window).outerWidth() < 990) {     
            _this.toggleClass('active');
            if (_this.hasClass('active')) {   
                _this.next('.yet-drop-content').css({
                    'display' : 'flex'
                });
            } else {
                _this.next('.yet-drop-content').css({
                    'display' : 'none'
                });
            }
        }
    })    
    $('.tel-drop .tel-text').click(function() {
        let _this = $(this);
        if ($(window).outerWidth() < 990) {     
            _this.toggleClass('active');
            if (_this.hasClass('active')) {   
                _this.next('.tel-drop-content').css({
                    'display' : 'flex'
                });
                $('.tel-drop-info').show();
                $('.request-call-form').hide();
            } else {
                _this.next('.tel-drop-content').css({
                    'display' : 'none'
                });
            }
        }
    })
    $('.sub-catalog .sub-level').click(function() {
        if ($(window).innerWidth() <= 990) {
            let firstLvl = $(this).find('.sub-catalog')[0]
            $(this).parent().addClass('opened-sub-catalog')
            $(this).addClass('opened-sub-level')
            $(firstLvl).css({
                'display': 'flex',
                'left': '-2px',
                'z-index': 100
            })
        }
    })

    function reset_menu_mobile() {
        $('.opened-sub-catalog').removeClass('opened-sub-catalog')
        $('.sub-level').removeClass('opened-sub-level')
        $('.sub-catalog').removeAttr('style');

    }
    /*END*/

    /*Закрытие поиск в битриксе*/
    $('.title-search-result').click(function(e) {
        if($(e.target).is('.title-search-result')){
            $('.title-search-result').css({display: 'none'})
        }
    })
    /*END*/

    /*Удаление товара из корзины(всплывашка)*/
      $('.basket .products .product .close').click(function() {
        let informer = $('.basket>.icon-wrapper .basket-count span')
        let headPrice = $('.basket .basket-text span:last-child')
        let bottomPrice = $('.basket .bottom-panel p')
        let strVal = informer.text()
        let parent = $(this).parents('.product')
        let productPrice = parseInt(parent.find('.price').text().replace(' ', ''))
        let hPrice = parseInt(headPrice.text().split(' руб.')[0].replace(' ', ''))
        hPrice = (hPrice - productPrice) + ' руб.'
        headPrice.text(hPrice)
        bottomPrice.text(hPrice)
        strVal = parseInt(strVal) - 1
        informer.text(strVal)
        parent.remove()
      })
    /*END*/

    /*Контроль за прокруткой всплывающих окон в шапке*/
      $('.user-icon-wrapper , .favorite-icon-wrapper, .basket').click(function(){
        // if ($(window).innerWidth() <= 990) 
        //     $('body').css('overflow', 'hidden')


      }/*, () => {
        if ($(window).innerWidth() <= 990)
            $('body').css('overflow', 'auto')
      }*/)
    /**/


    $(window).resize(function () {
        let height = $('.categories').innerWidth() / 3
        if (height >= 300)
            $('.categories').css('grid-auto-rows', `${height}px`)
    })

    $('.burger-menu-header__back').on('click', function(event) {
        event.preventDefault();
        reset_menu_mobile();
        $('.second-header').removeClass('catalog-opened');
        $('.sub-catalog').removeAttr('style');
    });
    $('.burger-menu-header__close').on('click', function(event) {
        event.preventDefault();
        reset_menu_mobile();
        $('.second-header').removeClass('catalog-opened');
        $('.sub-catalog').removeAttr('style');

        $('.sub-catalog').attr('style', '')
        $('.second-header').removeClass('second-header-mobile')

        if (!$('.second-header').hasClass('catalog-opened')) {
            $('.second-header').removeClass('catalog-opened');
        }
        if ($('.close-mobile-menu').is('.burger-mobile')) {
          $('body').css('overflow', 'hidden')
        } else {
          $('body').css('overflow', 'auto')
        }
    });

    $('body').on('click', '.product-favorites', function(event) {
        event.preventDefault();
        $(this).toggleClass('active');
    });
    $(window).trigger('resize');

    if ($('#map').length) {
      ymaps.ready(function () {

        map = new ymaps.Map('map', {
          center: [54.98322006972085, 82.89710949999991],
          zoom: 16,
          controls: []
        });

        var myPlacemark = new ymaps.Placemark([54.98322006972085, 82.89710949999991], 
            { 
                hintContent: 'Тут можно сделать адрес или что угодно',
                balloonContent: 'Тут можно сделать адрес или что угодно' 
            }, {
                iconLayout: 'default#image',
                iconImageHref: 'img/map_icon2x.png',
                iconImageSize: [85, 94],
                iconImageOffset: [-42, -94]
            });
        map.geoObjects.add(myPlacemark);
      });
    }
})