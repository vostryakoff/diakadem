'use strict';

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    prefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify-es').default,
    pug = require('gulp-pug'),
    stylus = require('gulp-stylus'),
    sourcemaps = require('gulp-sourcemaps'),
    cleanCSS = require('gulp-clean-css'),
    imagemin = require('gulp-imagemin'),
    rimraf = require('rimraf'),
    browserSync = require("browser-sync"),
    zip = require('gulp-zip'),
    babel = require("gulp-babel"),
    urlAdjuster = require('gulp-css-url-adjuster'),
    useref = require('gulp-useref'),
    plumber = require('gulp-plumber'),
    reload = browserSync.reload;

var path = {
    build: { //Тут мы укажем куда складывать готовые после сборки файлы
        html: 'build/',
        js: 'build/js/',
        jsLibs: 'build/js/vendor/',
        css: 'build/css/',
        cssLibs: 'build/css/vendor/',
        img: 'build/img/',
        fonts: 'build/css/fonts/',
        result: 'result/'
    },
    src: { //Пути откуда брать исходники
        html: 'src/*.pug', //Синтаксис src/*.html говорит gulp что мы хотим взять все файлы с расширением .html
        js: 'src/js/*.js',//В стилях и скриптах нам понадобятся только main файлы
        jsLibs: 'src/js/vendor/*.js',
        style: 'src/css/*.styl',
        cssLibs: 'src/css/vendor/*.css',
        img: 'src/img/**/*.*', //Синтаксис img/**/*.* означает - взять все файлы всех расширений из папки и из вложенных каталогов
        fonts: 'src/fonts/*.{eot,ttf,woff,woff2,svg}',
        buildToZip: 'build/**/*.*'
    },
    watch: { //Тут мы укажем, за изменением каких файлов мы хотим наблюдать
        html: 'src/**/*.pug',
        js: 'src/js/**/*.js',
        jsLibs: 'src/js/vendor/*.js',
        cssLibs: 'src/css/vendor/*.css',
        style: 'src/css/**/*.styl',
        img: 'src/img/**/*.*',
        fonts: 'src/fonts/*.{eot,ttf,woff,woff2,svg}'

    },
    clean: './build'
};

var config = {
    server: {
        baseDir: "./build"
    },
    tunnel: true,
    host: 'localhost',
    port: 9000,
    logPrefix: "by_Alex",
    open: false
};

gulp.task('html:build', function () {
    gulp.src(path.src.html) //Выберем файлы по нужному пути
        .pipe(plumber())
        .pipe(pug({
            pretty: true,
            locals: {pjson: require('./package')}
        }))
        .pipe(useref())
        .pipe(gulp.dest(path.build.html)) //Выплюнем их в папку build
        .pipe(reload({stream: true})); //И перезагрузим наш сервер для обновлений
});

gulp.task('html:prod:build', function () {
    gulp.src(path.src.html) //Выберем файлы по нужному пути
        .pipe(plumber())
        .pipe(pug(
            {
                pretty: true,
                locals: {pjson: require('./package')}
            }
        ))
        .pipe(gulp.dest(path.build.html)) //Выплюнем их в папку build
        .pipe(reload({stream: true})); //И перезагрузим наш сервер для обновлений
});

gulp.task('js:build', function () {
    gulp.src(path.src.js) //Найдем наш main файл
        .pipe(plumber())
        .pipe(sourcemaps.init()) //Инициализируем sourcemap
        .pipe(babel())
        .pipe(uglify()) //Сожмем наш js
        .pipe(sourcemaps.write()) //Пропишем карты
        .pipe(gulp.dest(path.build.js)) //Выплюнем готовый файл в build
        .pipe(reload({stream: true})); //И перезагрузим сервер
});

gulp.task('js:libs:build', function () {
    gulp.src(path.src.jsLibs) //Найдем наш main файл
        .pipe(plumber())
        .pipe(gulp.dest(path.build.jsLibs)) //Выплюнем готовый файл в build
        .pipe(reload({stream: true})); //И перезагрузим сервер
});

gulp.task('css:build', function () {
    gulp.src(path.src.style) //Выберем наш main.scss
        .pipe(plumber())
        .pipe(sourcemaps.init()) //То же самое что и с js
        .pipe(stylus({
              url: 'embedurl'
            })) //Скомпилируем
        .pipe(prefixer()) //Добавим вендорные префиксы
        .pipe(urlAdjuster({ //Исправляем пути подключения шрифтов
            replace:  ['../../fonts','fonts']
        }))
        // .pipe(cleanCSS({compatibility: 'ie10'})) //Сожмем
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.css)) //И в build
        .pipe(reload({stream: true}));
});
gulp.task('css:libs:build', function () {
    gulp.src(path.src.cssLibs) //Выберем наш main.scss
        .pipe(plumber())
        .pipe(sourcemaps.init()) //То же самое что и с js
        .pipe(cleanCSS()) //Сожмем
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.cssLibs)) //И в build
        .pipe(reload({stream: true}));
});

gulp.task('image:build', function () {
    gulp.src(path.src.img) //Выберем наши картинки
        .pipe(imagemin({ //Сожмем их
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            //use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest(path.build.img)) //И бросим в build
        .pipe(reload({stream: true}));
});

gulp.task('fonts:build', function() {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
});

gulp.task('build:to:result', ['prod:build'], function() {
    gulp.src(path.src.buildToZip)
        .pipe(zip('result.zip'))
        .pipe(gulp.dest(path.build.result))
});

gulp.task('dev:build', [
    'html:build',
    'js:build',
    'js:libs:build',
    'css:build',
    'css:libs:build',
    'fonts:build',
    'image:build'
]);

gulp.task('prod:build', [
    'html:prod:build',
    'js:build',
    'js:libs:build',
    'css:build',
    'css:libs:build',
    'fonts:build',
    'image:build'
]);

gulp.task('zip', [
    'build:to:result'
]);

gulp.task('watch', function(){
    watch([path.watch.html], function(event, cb) {
        gulp.start('html:build');
    });
    watch([path.watch.style], function(event, cb) {
        gulp.start('css:build');
    });
    watch([path.watch.cssLibs], function(event, cb) {
        gulp.start('css:libs:build');
    });
    watch([path.watch.jsLibs], function(event, cb) {
        gulp.start('js:libs:build');
    });
    watch([path.watch.js], function(event, cb) {
        gulp.start('js:build');
    });
    watch([path.watch.img], function(event, cb) {
        gulp.start('image:build');
    });
    watch([path.watch.fonts], function(event, cb) {
        gulp.start('fonts:build');
    });
});

gulp.task('webserver', function () {
    browserSync(config);
});

gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});

gulp.task('default', ['dev:build', 'webserver', 'watch']);
